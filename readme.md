**Content of this repository/**

***Visual studio information.***

Compiled with Microsoft Visual Studio 2019, version 16.3.1
.Net Core 3.0


**Sql Server information**

Sql Server Management Studio v18.3


**Postman**

Postman collection 2.1


**Database**

Schema: dbo
Tables: 
Action /* Describes 3 actions: add, update and delete */
Cart /* Store the shoppings */
Like /* Store the likes */
Logs /* Store the actions performed by users */
Product /* Store products */
Session /* Stores the unique identificator to handled request */
User /* Store username, password and role */

**Users to test**
user: benjamin
password: 123

user: client
password: 123


**Security schema**

Password: Stored using `Bcrypt`.
ApplaudoRoleProvider: Identify annotation over controllers `[ApplaudoRoleProvier("Admin")]`. Works to identify methods are accesible only for Administrators.

















