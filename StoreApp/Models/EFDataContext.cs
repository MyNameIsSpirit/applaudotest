﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StoreApp.Models.DB;

namespace StoreApp.Models
{
    public class EFDataContext : DbContext
    {
        public DbSet<DB.Action> Actions { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Log> Logs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"data source=DESKTOP-9BQ11S0; initial catalog=TestDB;persist security info=True;user id=sa");
            optionsBuilder.UseSqlServer(@"data source=DESKTOP-9BQ11S0; initial catalog=TestDB;persist security info=True; Integrated Security=SSPI; ");

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Like>()
                .HasOne<Product>(x => x.ProductInfo)
                .WithMany(x => x.Likes)
                .HasForeignKey(x => x.IdProduct)
                .IsRequired();

        }

    }
}
