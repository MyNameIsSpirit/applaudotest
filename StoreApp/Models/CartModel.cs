﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreApp.Models
{
    public class CartModel
    {
        public int id { get; set; }
        public int qty { get; set; }

        public CartModel()
        {

        }

        public CartModel(int id, int qty)
        {
            this.id = id;
            this.qty = qty;
        }
    }
}
