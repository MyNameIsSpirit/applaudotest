﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace StoreApp.Models.DB
{
    [Table("Session", Schema = "dbo")]
    public class Session
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("UserInfo")]
        public int IdUser { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string Token { get; set; }

        public virtual User UserInfo { get; set; }
    }
}
