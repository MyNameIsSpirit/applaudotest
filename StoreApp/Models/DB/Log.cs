﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreApp.Models.DB
{
    public class Log
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("UserInfo")]
        public int IdUser { get; set; }

        public virtual User UserInfo { get; set; }

        [ForeignKey("ProductInfo")]
        public int IdProduct { get; set; }

        public virtual Product ProductInfo { get; set; }

        [ForeignKey("ActionInfo")]
        public int IdAction { get; set; }

        public virtual Action ActionInfo { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime EventDate { get; set; }
    }
}
