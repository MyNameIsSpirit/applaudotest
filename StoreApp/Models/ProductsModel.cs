﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreApp.Models
{
    public class ProductsModel
    {
        public int IdProduct { get; set; }
        public int Likes { get; set; }
        public string Product { get; set; }
        public float Price { get; set; }
        public int Stock { get; set; }
    }
}
