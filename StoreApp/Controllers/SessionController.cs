﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StoreApp.Configuration;
using StoreApp.Models;
using StoreApp.Models.DB;
using MiniGuids;

namespace StoreApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SessionController : ControllerBase
    {
        protected EFDataContext _dbContext = new EFDataContext();

        [HttpPost]
        public IActionResult LogIn(string username, string password)
        {
            User user = _dbContext.Users.Where(x => x.Username == username).FirstOrDefault();
            if(user == null)
                return new ObjectResult(new BaseModel { Success = false, Message = "User not found." }) { StatusCode = 404 };

            if( !Hashing.ValidatePassword(password, user.Password)) 
                return new ObjectResult(new BaseModel { Success = false, Message = "Wrong password." }) { StatusCode = 404 };
    
            Session session = new Session
            {
                IdUser = user.Id,
                Token = MiniGuid.NewGuid().ToString()
            };

            _dbContext.Sessions.Add(session);
            _dbContext.SaveChanges();

            return new ObjectResult(new BaseModel { Success = true, Message = session.Token }) { StatusCode = 200 };

        }

        [HttpPatch]
        public IActionResult LogOut(String hash)
        {
            Session session = _dbContext.Sessions.Where(x => x.Token == hash).FirstOrDefault();

            if (session == null)
                return new ObjectResult(new BaseModel { Success = false, Message = "Session not found." }) { StatusCode = 404 };

            _dbContext.Sessions.Remove(session);
            _dbContext.SaveChanges();

            return new ObjectResult(new BaseModel { Success = true, Message = "Session removed." }) { StatusCode = 200 };

        }
    }
}