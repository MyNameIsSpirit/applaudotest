﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StoreApp.Models;
using StoreApp.Models.DB;
using StoreApp.Security;
using StoreApp.Services;

namespace StoreApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StoreController : ControllerBase
    {
        protected EFDataContext _dbContext = new EFDataContext();
        protected readonly LogService logService = new LogService();

        [HttpPost]
        [ApplaudoRoleProvider("Admin")]
        public IActionResult Add(string name, float price, int stock)
        {
            if(name == null || price == null || stock == null)
                return new ObjectResult(new BaseModel { Success = false, Message = "Must complete 'name', 'price' and 'stock' to add a new product. " }) { StatusCode = 403 };

            if(stock < 0)
                return new ObjectResult(new BaseModel { Success = false, Message = "Stock should be higher or equals to 0. " }) { StatusCode = 403 };

            if (price <= 0)
                return new ObjectResult(new BaseModel { Success = false, Message = "Price should be higher than 0. " }) { StatusCode = 403 };

            var product = new Product
            {
                Deleted = false,
                Name = name,
                Price = price,
                Stock = stock
            };

            _dbContext.Products.Add(product);
            _dbContext.SaveChanges();

            logService.Save(_dbContext.Sessions.FirstOrDefault(x => x.Token == Request.Headers["token"].ToString()).IdUser, product.Id, 1);

            return new ObjectResult(new BaseModel { Success = true, Message = "Added with id: " + product.Id }) { StatusCode = 404 };
        }

        [HttpPost]
        [Route("Stock")]
        [ApplaudoRoleProvider("Admin")]
        public IActionResult Stock(int id, int stock)
        {
            if (id == null || stock == null)
                return new ObjectResult(new BaseModel { Success = false, Message = "Must complete 'stock' to update a new product. " }) { StatusCode = 403 };

            if (stock < 0)
                return new ObjectResult(new BaseModel { Success = false, Message = "Stock should be higher or equals to 0. " }) { StatusCode = 403 };

            Product product = _dbContext.Products.FirstOrDefault(x => x.Id == id);

            if (product == null)
                return new ObjectResult(new BaseModel { Success = false, Message = "Product not found. " }) { StatusCode = 404 };

            product.Stock = stock;

            _dbContext.Products.Update(product);
            _dbContext.SaveChanges();

            logService.Save(_dbContext.Sessions.FirstOrDefault(x => x.Token == Request.Headers["token"].ToString()).IdUser, product.Id, 3);

            return new ObjectResult(new BaseModel { Success = true, Message = "Stock updated." }) { StatusCode = 404 };
        }

        [HttpPost]
        [Route("price")]
        [ApplaudoRoleProvider("Admin")]
        public IActionResult Price(int id, float price)
        {
            if (id == null || price == null)
                return new ObjectResult(new BaseModel { Success = false, Message = "Must complete 'price' to add a new product. " }) { StatusCode = 403 };

            if (price <= 0)
                return new ObjectResult(new BaseModel { Success = false, Message = "Price should be higher to 0. " }) { StatusCode = 403 };

            Product product = _dbContext.Products.FirstOrDefault(x => x.Id == id);

            if (product == null)
                return new ObjectResult(new BaseModel { Success = false, Message = "Product not found. " }) { StatusCode = 404 };

            product.Price = price;

            _dbContext.Products.Update(product);
            _dbContext.SaveChanges();

            logService.Save(_dbContext.Sessions.FirstOrDefault(x => x.Token == Request.Headers["token"].ToString()).IdUser, product.Id, 3);

            return new ObjectResult(new BaseModel { Success = true, Message = "Price updated." }) { StatusCode = 404 };
        }


        [HttpDelete("{id}")]
        [ApplaudoRoleProvider("Admin")]
        public IActionResult Delete(int id)
        {
            var product = _dbContext.Products.FirstOrDefault(item => item.Id == id);

            if (product == null)
                return new ObjectResult(new BaseModel {Success = false, Message = "Product not found" }) { StatusCode = 404 };


            product.Deleted = true;
            _dbContext.Products.Update(product);
            _dbContext.SaveChanges();

            logService.Save(_dbContext.Sessions.FirstOrDefault(x => x.Token == Request.Headers["token"].ToString()).IdUser, product.Id, 2);

            return new ObjectResult(new BaseModel { Success = true, Message = "Deleted (logically)" }) { StatusCode = 200 };
        }

        [HttpGet]
        public IEnumerable<ProductsModel> Get(int? page, string sortby)
        {
            int _page = (page == null || page.Value < 1) ? 1 : page.Value;
            int pageSize = 10;

            var data = new List<Product>();


            var query = this._dbContext.Products
                .Include(x => x.Likes)
                .Where(x => !x.Deleted);

            if (sortby == "likes")
                query.OrderBy(x => x.Likes.Count());

            data = query.Skip( (_page - 1) * 10 )
                .Take(pageSize)
                .ToList();

            return data.Select(item => new ProductsModel
            {
                IdProduct = item.Id,
                Product = item.Name,
                Price = item.Price,
                Stock = item.Stock,
                Likes = item.Likes.Count()
            })
            .ToArray();
        }


        [HttpPost]
        [Route("buy")]
        //[ApplaudoRoleProvider("Admin")]
        public IActionResult Buy(String ids, String qtys, string token)
        {
            try
            {
                var identifiers = new List<int>();
                var quantities = new List<int>();

                var cart = new List<CartModel>();

                if( ids.Split(",").Count() != qtys.Split(",").Count() && ids.Split(",").Count() > 0)
                {
                    return new ObjectResult(new BaseModel { Success = true, Message = "Wrong format." }) { StatusCode = 404 };
                }

                var _ids = ids.Split(",");
                var _qtys = qtys.Split(",");

                for(var i = 0; i < _ids.Length; i++)
                {
                    var id = Convert.ToInt32(_ids[i]);
                    var qty = Convert.ToInt32(_qtys[i]);

                    cart.Add(new CartModel(id, qty));
                }

                Session session = _dbContext.Sessions.FirstOrDefault(x => x.Token == token);

                if (session == null)
                    return new ObjectResult(new BaseModel { Success = true, Message = "Token no valid." }) { StatusCode = 404 };

                String error = "";

                foreach (var item in cart)
                {
                    Product product = _dbContext.Products.FirstOrDefault(x => x.Id == item.id);
                    if(product == null)
                    {
                        error += " - The product with id '" + item.id + "' is not valid.";
                    } else if(product.Stock >= item.qty) { 
                        Cart element = new Cart {
                            EventDate = DateTime.Now,//new DateTime(),
                            IdProduct = item.id,
                            IdUser = session.IdUser,
                            Quantity = item.qty
                        };
                    } else
                    {
                        error += " - The stock for the product with id '" + item.id + "' is " + product.Stock;
                    }
                }

                if(error != "")
                {
                    return new ObjectResult(new BaseModel { Success = false, Message = error }) { StatusCode = 404 };

                }
                
            } catch
            {
                return new ObjectResult(new BaseModel { Success = false, Message = "An error ocurred parsing the information." }) { StatusCode = 404 };
            }
            
            return new ObjectResult(new BaseModel { Success = true, Message = "Articles registered." }) { StatusCode = 200 };
        }



        [HttpPost]
        [Route("Like")]
        //[ApplaudoRoleProvider("Admin")]
        public IActionResult Like(string token, int idProduct)
        {
            Session session = _dbContext.Sessions.FirstOrDefault(x => x.Token == token);
            if (session == null)
                    return new ObjectResult(new BaseModel { Success = true, Message = "Token no valid." }) { StatusCode = 404 };

            Product product = _dbContext.Products.FirstOrDefault(x => x.Id == idProduct);
            if(product == null)
                return new ObjectResult(new BaseModel { Success = true, Message = "Product not found." }) { StatusCode = 404 };


            Like like = new Like
            {
                EventDate = DateTime.Now,
                IdProduct = idProduct,
                IdUser = session.IdUser
            };

            _dbContext.Likes.Add(like);
            _dbContext.SaveChanges();

            return new ObjectResult(new BaseModel { Success = true, Message = "Lied." }) { StatusCode = 200 };
        }

    }
}