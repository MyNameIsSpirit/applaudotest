﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreApp.Configuration
{
    public class SwaggerConfiguration
    {
        public string JsonRoute { get; set; }
        public string Description { get; set; }
        public String UIEndpoint { get; set; }
    }
}
