﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BCrypt.Net;

namespace StoreApp.Configuration
{
    public class Hashing
    {
        public static string HashPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }

        public static bool ValidatePassword(string userSubmittedPassword, string hashedPassword)
        {
            return BCrypt.Net.BCrypt.Verify(userSubmittedPassword, hashedPassword);
        }
    }
}
