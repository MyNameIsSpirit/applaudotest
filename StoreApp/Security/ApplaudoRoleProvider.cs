﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using StoreApp.Models;
using StoreApp.Models.DB;

namespace StoreApp.Security
{
    public class ApplaudoRoleProvider : ActionFilterAttribute
    {
        private readonly string _operation;
        protected EFDataContext _dbContext = new EFDataContext();

        public ApplaudoRoleProvider(string operation) : base()
        {
            _operation = operation;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var request = context.HttpContext.Request;
            String token = null;

            if(request.Path.Value.Contains("session") )
            {
                return;
            }

            var headers = request.Headers;

            if (headers.ContainsKey("token"))
            {
                token = headers["token"].ToString();
            }

            if(token == null)
                context.Result = new BadRequestObjectResult("Not allowed");

            Session session = _dbContext.Sessions.FirstOrDefault(x => x.Token == token);
                
            if(session == null)
                context.Result = new BadRequestObjectResult("Not allowed");

            if (
                request.Path.Value.Contains("store/stock") || 
                request.Path.Value.Contains("store/price") ||
                (request.Path.Value.Contains("store") && request.Method.ToUpper() == "POST") ||
                request.Method.ToUpper() == "DELETE"
                )
            {
                //evaluate if is admin
                User user = _dbContext.Users.FirstOrDefault(x => x.Id == session.IdUser);
                if(user != null && user.Role !=_operation )
                    context.Result = new BadRequestObjectResult("Not allowed");
            } 
        }
    }
}
