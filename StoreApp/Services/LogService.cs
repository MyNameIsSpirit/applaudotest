﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StoreApp.Models;
using StoreApp.Models.DB;

namespace StoreApp.Services
{
    public class LogService
    {
        protected EFDataContext _dbContext = new EFDataContext();

        public void Save(int idUser, int idProduct, int idAction)
        {
            var date = DateTime.Now;
            Log log = new Log
            {
                IdAction = idAction,
                IdProduct = idProduct,
                IdUser = idUser,
                EventDate = date
            };

            _dbContext.Logs.Add(log);
            _dbContext.SaveChanges();
        }
    }
}
